import { Router } from '@angular/router';
import { MessageService } from './../_services/message.service';
import { MatchService } from './../_services/match.service';
import { Match } from './../_models/Match';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {

  currentMatch: Match;
  constructor(private router: Router,
    private matchservice: MatchService,
    private messageservice: MessageService) { }

  ngOnInit() {
    this.currentMatch = JSON.parse(localStorage.getItem('currentMatch'));
  }

  updateprofile(): void {
    this.matchservice.updateMatch(this.currentMatch).subscribe(match => {
      localStorage.setItem("currentMatch", JSON.stringify(match));
      this.router.navigate(["matches"]);
    });
  }

}
