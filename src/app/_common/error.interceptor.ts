import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MessageService } from '../_services/message.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private messageService: MessageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      //alert(JSON.stringify(err));
      let error: string;
      if (err.error.apierror) {
        error = err.error.apierror.message || err.error.apierror.status;
      } else {
        error = JSON.stringify(err);
      }
      this.messageService.add(error);
      console.log(error);
      return throwError(err);
    }));
  }
}
