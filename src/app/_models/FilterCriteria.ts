import { Range } from "./Range";
import { LocationDistance } from "./LocationDistance";

export class FilterCriteria {
  currentId: number;
  hasPhoto: boolean;
  inContact: boolean;
  favourite: boolean;
  compatibilityScore: Range = new Range();
  age: Range = new Range();
  height: Range = new Range();
  locDist: LocationDistance = new LocationDistance();
}
