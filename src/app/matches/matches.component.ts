import { MatchContact } from './../_models/MatchContact';
import { MessageService } from './../_services/message.service';
import { Contact } from './../_models/Contact';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Match } from '../_models/Match';
import { FilterCriteria } from '../_models/FilterCriteria';
import { MatchService } from '../_services/match.service';
import { Router } from '@angular/router';
import { UiServiceService } from '../ui-service.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent {

  currentMatch: Match;
  matches: Match[];
  filter: FilterCriteria = new FilterCriteria();
  ageRange: number[] = [18, 95];
  csRange: number[] = [1, 99];
  heightRange: number[] = [135, 210];
  dist: number = 30;

  showMenu = false;
  darkModeActive: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private matchService: MatchService,
    private router: Router,
    public ui: UiServiceService,
    private messageService: MessageService) { }

  ngOnInit(): void {

    this.currentMatch = JSON.parse(localStorage.getItem('currentMatch'));
    this.loadInitialMatches();

    this.ui.darkModeState.subscribe((value) => {
      this.darkModeActive = value;
    });

  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  modeToggleSwitch() {
    this.ui.darkModeState.next(!this.darkModeActive);
  }

  loadInitialMatches(): void {
    let fc: FilterCriteria = new FilterCriteria();
    if (this.currentMatch.id) {
      fc.currentId = this.currentMatch.id;
    } else {
      fc.currentId = 0;
    }
    this.matchService.getMatches(fc).subscribe(data => {
      this.matches = data;
    });
  }

  filterMatches(event: any, type: string): void {
    this.filter.currentId = this.currentMatch.id;
    if (type === 'age') {
      this.filter.age.min = event[0];
      this.filter.age.max = event[1];
    } else if (type === 'cs') {
      this.filter.compatibilityScore.min = event[0];
      this.filter.compatibilityScore.max = event[1];
    } else if (type === 'height') {
      this.filter.height.min = event[0];
      this.filter.height.max = event[1];
    } else if (type === 'dist') {
      this.filter.locDist.distanceInKm = event;
      this.filter.locDist.lat = this.currentMatch.city.lat;
      this.filter.locDist.lon = this.currentMatch.city.lon;
    }
    this.matchService.getMatches(this.filter).subscribe(data => {
      this.matches = data;
    });
  }

  addToContact(event: any, toBeAddedContact: Match): void {
    let matchContact: MatchContact = new  MatchContact();
    matchContact.contact = toBeAddedContact;
    matchContact.match = this.currentMatch;
    this.matchService.addToContact(matchContact).subscribe(data => {
      this.messageService.add(data.displayName + ' added to your contact !!');
    });
  }

  removeFromContact(event: any, toBeRemovedContact: Match): void {
    let matchContact: MatchContact = new  MatchContact();
    matchContact.contact = toBeRemovedContact;
    matchContact.match = this.currentMatch;
    this.matchService.removeFromContact(matchContact).subscribe(data => {
      this.messageService.add(data.displayName + ' removed from your contact !!');
      this.matches.splice(this.matches.indexOf(data), 1);
    });
  }

  returnToLanding(): void {

    this.router.navigate(['landing']);
  }

  }
